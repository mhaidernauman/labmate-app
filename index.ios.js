// /** @format */

// import {AppRegistry} from 'react-native';
// import App from './App';
// import {name as appName} from './app.json';
// import DeviceInfo from 'react-native-device-info';
// import DatePicker from 'react-native-datepicker'
// import DNS from 'react-native-fs'
// import * as Animatable from 'react-native-animatable';
// import RNFetchBlob from 'react-native-fetch-blob'
// import Spinner from 'react-native-loading-spinner-overlay';



// AppRegistry.registerComponent(appName, () => App);

import { AppRegistry } from 'react-native';
import Routing from './app/routes/Routing';


global.___DEV___ = false;
console.disableYellowBox = true;

// import App from './App';

AppRegistry.registerComponent('reactTutorialApp', () => Routing );
