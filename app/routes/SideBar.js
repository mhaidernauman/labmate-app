import React from "react";
import { Image, StatusBar } from "react-native";
import { Container, Content, Text, List, ListItem } from "native-base";

// Dummy Routes
const routes = ["Promotions", "Events"];

export default class SideBar extends React.Component
{
    constructor( props )
    {
        super(props);
    }

    render() 
    {
        let getProps        = this.props;
        let csRoutes        = this.props.navigation.state.routes;
        let activeRoutes    = this.props.activeItemKey;
        let position        = this.props.drawerPosition;

        csRoutes = csRoutes.filter((item) => {
            
            if( item.routeName !='ResearchNews' &&
                item.routeName !='ProductNews' &&
                item.routeName !='PromotionDetail' && 
                item.routeName !='ProductList' &&
                item.routeName !='ProductDetail' &&
                item.routeName !='GetEnquiry' &&
                item.routeName !='CertificateOfAnalysis' &&
                item.routeName !='GetAnAppoinment' && 
                item.routeName !='EventDetail' && 
                item.routeName !='EventRegistration' && 
                item.routeName !='YourSelection' &&
                item.routeName !='ProfileEdit' &&
                item.routeName !='ChangePassword' &&
                item.routeName != 'PDFViewComponent'
             )
            {
                return item;
            }

        });

        // console.warn( csRoutes );

        // console.warn(getProps);
        // console.warn(csRoutes);
        // console.warn(activeRoutes);
        // console.warn(position);

        return (
            <List
                dataArray={csRoutes}
                renderRow={data => {
                return (
                    <ListItem style={{ borderBottomColor: 'lightgrey', borderBottomWidth:2, width:245 }} button
                        onPress={() => {
                            const {navigate} = this.props.navigation;
                            navigate(data.routeName);
                        }}>

                        {(

                        ( data.key  == 'Home' ) ?
                            <Image source={require('./../img/icon/home-icon.png')} style={{ width:15, height:15, marginHorizontal:10, marginVertical:5 }} />
                        :
                        
                        ( data.key  == 'Request for Quotation' ) ?
                            <Image source={require('./../img/icon/id-enquiry-icon.png')} style={{ width:13, height:15, marginHorizontal:10, marginVertical:5 }} />
                        :                            

                        ( data.key  == 'Product Lines' ) ?
                            <Image source={require('./../img/icon/product-line-icon.png')} style={{ width:15, height:15, marginHorizontal:10, marginVertical:5 }} />
                        :
                        
                        ( data.key  == 'Support Hub' ) ?
                            <Image source={require('./../img/icon/support-icon.png')} style={{ width:15, height:15, marginHorizontal:10, marginVertical:5 }} />
                        :
                        
                        ( data.key  == 'Newsroom' ) ?
                            <Image source={require('./../img/icon/promotions-icon.png')} style={{ width:15, height:15, marginHorizontal:10, marginVertical:5 }} />
                        :

                        ( data.key  == 'Events' ) ?
                            <Image source={require('./../img/icon/id-event-icon.png')} style={{ width:15, height:15, marginHorizontal:10, marginVertical:5 }} />
                        :

                        ( data.key  == 'Your Selection' ) ?
                            <Image source={require('./../img/icon/selection-icon.png')} style={{ width:15, height:15, marginHorizontal:10, marginVertical:5 }} />
                        :

                        ( data.key  == 'About Us' ) ?
                            <Image source={require('./../img/icon/about-us-icon.png')} style={{ width:15, height:15, marginHorizontal:10, marginVertical:5 }} />
                        :

                        ( data.key  == 'Contact Us' ) ?
                            <Image source={require('./../img/icon/contact-us.png')} style={{ width:15, height:15, marginHorizontal:10, marginVertical:5 }} />
                        :

                        ( data.key  == 'Logout' ) ?
                            <Image source={require('./../img/icon/log-out-icon.png')} style={{ width:15, height:15, marginHorizontal:10, marginVertical:5 }} />
                        :
                            <Image source={require('./../img/icon/name-icon.png')} style={{ width:15, height:15, marginHorizontal:10, marginVertical:5 }} />

                        )}
                        
                        <Text style={{ color: 'grey', fontFamily: 'Roboto-Regular', fontWeight:'bold', fontSize: 14 }} >{data.key}</Text>
                    </ListItem>
                );
            }}
        />
        );
    }
}