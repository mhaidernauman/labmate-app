import React from 'react';
import { StackNavigator, DrawerNavigator, DrawerItems} from 'react-navigation';
import {Image, TouchableOpacity} from 'react-native';
import {Container, Content, Header, Body, Icon, View, Text} from 'native-base';

/**
 * Firebase Notification :: Top Bar Notification
 */
import { Platform } from 'react-native';

/**
 * DrawerNavigator :: Sidebar-Navigation && Pages
 */

import QuickQuotes from './../Sidebar/QuickQuotes';

import Productlines from './../Sidebar/Productlines';
	import ProductList from './../Sidebar/ProductList';
	import ProductDetail from './../Sidebar/ProductDetail';

import QuickSupports from './../Sidebar/QuickSupports';
	import GetEnquiry from './../Sidebar/GetEnquiry';
	import GetAnAppoinment from './../Sidebar/GetAnAppoinment';
	import CertificateOfAnalysis from './../Sidebar/CertificateOfAnalysis';

import Newsroom from './../Sidebar/Newsroom';
	import ResearchNews from './../Sidebar/ResearchNews';
	import ProductNews from './../Sidebar/ProductNews';
	import PromotionDetail from './../Sidebar/PromotionDetail';

import Events from './../Sidebar/Events';
	import EventDetail from './../Sidebar/EventDetail';
	import EventRegistration from './../Sidebar/EventRegistration';

// import Brands from './../Sidebar/Brands';
import YourSelection from './../Sidebar/YourSelection';
import Aboutus from './../Sidebar/Aboutus';
import Contactus from './../Sidebar/Contactus';
import ProfileEdit from './../Sidebar/ProfileEdit';
import ChangePassword from './../Sidebar/ChangePassword';
import PDFViewComponent from '../Sidebar/PDFViewComponent';

import Logout from './../Sidebar/Logout';

import SideBar from "./SideBar.js";
import SidebarHeader from "./../Sidebar/SidebarHeader";

const CustomDrawerContentComponent = (props) => (
	
	<Container style={{ flex:1 }}>
		<Header style={{ height:130 , backgroundColor: 'white', paddingTop: 30 }}>
			<SidebarHeader prop_property = {props} />
		</Header>
		<Content>
			
			<SideBar {... props}  />

			{/* <DrawerItems {... props} /> */}
			
		</Content>
	</Container>
);


const AppDrawer = DrawerNavigator({
	Home: {screen: Dashboard},
	'About Us': {screen: Aboutus},
	'Product Lines': {screen: Productlines},
	'Request for Quotation': {screen: QuickQuotes},

	'Support Hub': {screen: QuickSupports},
	Newsroom: {screen: Newsroom},

	Events: {screen: Events},
	'Contact Us': {screen: Contactus},
	Logout: {screen: Logout},

	
	// OnlyTest 
	ResearchNews: {screen: ResearchNews},
	ProductNews: {screen: ProductNews},
	ProductList: {screen: ProductList},
	ProductDetail: {screen: ProductDetail},
	GetEnquiry: {screen: GetEnquiry},
	GetAnAppoinment: {screen: GetAnAppoinment},
	CertificateOfAnalysis: {screen: CertificateOfAnalysis},
	YourSelection: {screen: YourSelection},
	EventDetail: {screen: EventDetail},
	EventRegistration: {screen: EventRegistration},
	PromotionDetail: {screen: PromotionDetail},
	ProfileEdit: {screen: ProfileEdit},
	ChangePassword: {screen: ChangePassword},
	PDFViewComponent: {screen: PDFViewComponent}
	
}, {
	// initialRouteName: 'Dashboard',
	drawerPosition: 'Left',
	contentComponent: CustomDrawerContentComponent,
	drawerOpenRoute: 'DrawerOpen',
	drawerCloseRoute: 'DrawerClose',
	drawerToggleRoute: 'DrawerToggle',
});


/**
 * StackNavigator :: Normal-Navigation && Pages
 */

import Logo from './../Roots/Logo';
import Login from './../Roots/Login';
import Signup from './../Roots/Signup';
import Dashboard from './../Roots/Dashboard';

const Navigation = StackNavigator({
	Logo: {screen: Logo},
	Login: {screen: Login},
	Signup: {screen: Signup},

	ProfileEdit: {screen: ProfileEdit},
	
	// Sub Pages
	ProductList: {screen: ProductList},
	ProductDetail: {screen: ProductDetail},

	// Sub Pages
	GetEnquiry: {screen: GetEnquiry},
	GetAnAppoinment: {screen: GetAnAppoinment},

	// Sub Pages
	EventDetail: {screen: EventDetail},
	EventRegistration: {screen: EventRegistration},

	// Sub Pages
	PromotionDetail: {screen: PromotionDetail},

	Dashboard: AppDrawer
},
{
	headerMode: 'none',
    navigationOptions: {
        headerVisible: false,
    }
});


/**
 * Application Routing
 * Application Firebase Notification
 * 
 */
export default class Routing extends React.Component
{
	render() {
		return (
			<Navigation />
			// <AppDrawer />
			// <ProfileEdit />
			// <ProductDetail />
			// <Signup />
			// <Login />
			// <Dashboard />
			// <Testscreen />
		);
	}
}