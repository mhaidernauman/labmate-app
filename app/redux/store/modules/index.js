import { UserState, user } from './user'

/**
 * Root states.
 */
/*export type States = {
  app: AppState,
  user: UserState
}*/

/**
 * Root reducers.
 */
export const reducers = {
  user: user.reducer
}

/**
 * Root actions.
 */
export const actions = {
  user: user.actions
}

export { user }