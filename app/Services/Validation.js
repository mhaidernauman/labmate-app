export default class Validation
{
    static isStringAndSpeace( text )
    {
        let reg = /^[a-zA-Z\s]*$/;

        return reg.test(text);
    }

    static isOnlyString( text )
    {
        let reg = /^[a-zA-Z\s]*$/;

        return reg.test(text);
    }

    static isOnlyStringNumeric( text )
    {
        let reg = /^[Aa-z0-9]+$/;

        return reg.test(text);
    }

    static isEmail(email)
    {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        return reg.test(email);
    }

    static isNumber( text )
    {
        //let reg = /^-?\d+\?\d*$/;
        let reg = /^[0-9]+$/;

        return reg.test(text);
    }

    /**
    * ^         Start of string
    * [a-z0-9]  a or b or c or ... z or 0 or 1 or ... 9
    * +         one or more times (change to * to allow empty string)
    * $         end of string    
    * /i        case-insensitive
     */
    static isPassword( text )
    {
        //let reg = /^-?\d+\?\d*$/;
        let reg = /^[a-z0-9]+$/i;

        return reg.test(text);
    }

   static isNotAllowSpecialCharacters( text )
   {
       let reg = /[^a-zA-Z0-9 !-.,#]/;

       return reg.test(text);
   }
}
