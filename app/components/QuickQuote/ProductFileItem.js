import React from 'react';
import {View, Image, Text, StyleSheet, TouchableOpacity} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import Checkbox from 'react-native-custom-checkbox';

// Assets
import styles from './../../styles/QuickQuotos';

export default class ProductFileItem extends React.Component
{
    render()
    {
        let row             = this.props.row;
        let row_source      = row.file_url;
        let flatListIndex   = this.props.index; 

        return (
            <View style={{flexDirection: 'row', marginTop:20}}> 
                <View style={{width:260}}>
                    <TouchableOpacity onPress={ this.props.handleOfModalImage.bind(this, row_source) }>
                        <Text style={{ paddingTop:8, paddingBottom:12, paddingLeft:20, fontFamily: 'Roboto-Bold', fontWeight: '500', fontSize:16, color:'red' }}>
                            { row.file_name }
                        </Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity style={{ marginVertical:12, marginHorizontal:30 }} onPress={ this.props.handleOfDelFile.bind(this, flatListIndex) }>
                        <Icon name="times-circle" size={20} color="red" />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}