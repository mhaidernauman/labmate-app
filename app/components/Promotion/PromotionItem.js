import React from 'react';
import {View, Image, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Config from './../../Services/Config';

import * as Animatable from 'react-native-animatable';

// Assets
import StyleGridList from './../../styles/GridList';

export default class PromotionItem extends React.Component
{
    constructor( props )
    {
        super(props);
    }

    render()
    {
        let item_promotion = this.props.promotion_item;
        let item_promotion_last = (item_promotion[1]);

        return (
            <View style={ StyleGridList.boxRow }>

                <TouchableOpacity 
                     style={{ flex:1 }}
                        onPress={ () => this.props.navigate('PromotionDetail', { promotion_id: item_promotion[0].id}) }
                    >
                    <Animatable.View animation="flipInX" style={ StyleGridList.item }>

                        <Image 
                            source={{uri: Config.base_url() + item_promotion[0].image_chunks.image_url }}
                            style={{ height: 155, borderRadius: 5 }} 
                        />
                        <View style={{backgroundColor:'red', width:"100%", height:40, opacity: 0.4, position:'absolute', borderRadius: 5, top:115}}/>
                        <Text style={ StyleGridList.itemText }>{ item_promotion[0].title }</Text>
                    </Animatable.View>
                    
                </TouchableOpacity>

                {
                (( item_promotion_last )
                ?
                (<TouchableOpacity style={{ flex:1 }}
                    onPress={ () => this.props.navigate('PromotionDetail', { promotion_id: item_promotion[1].id}) }
                >
                    <Animatable.View animation="flipInX" style={ StyleGridList.item }>
                        
                        <Image 
                            source={{uri: Config.base_url() + item_promotion[1].image_chunks.image_url }}
                            style={{ height: 155, borderRadius: 5 }} 
                        />
                        <View style={{backgroundColor:'red', width:"100%", height:40, opacity: 0.4, position:'absolute', borderRadius: 5, top:115}}></View>
                        <Text style={ StyleGridList.itemText }>{ item_promotion[1].title }</Text>
                    </Animatable.View>
                    
                </TouchableOpacity>)
                :
                null
                )
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({

	bodyContainer:{
		flexDirection:'row',
		margin:5,
		justifyContent:'center',
	}
});