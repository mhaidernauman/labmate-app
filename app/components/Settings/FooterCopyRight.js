import React from 'react';
import { View, Text } from 'react-native';

export default class FooterCopyRight extends React.Component
{
    render()
    {
        return (
        	<View style={{
		        // flex: 1,
		        // flexDirection: 'column',
		        // justifyContent: 'center',
		        // alignItems: 'center',
		        marginTop: 5,
				// marginBottom: 1,
				// backgroundColor: 'rgba(52, 52, 52, 0)',
				// borderWidth: 0
		     }}>
	            <Text style={{ color: 'black', fontFamily: 'verdana', fontSize: 14, fontWeight:'bold', letterSpacing: 0 }}>Musaji Adam and Sons </Text>
	            <Text style={{ color: 'black', fontFamily: 'verdana', fontSize: 10, fontWeight:'bold', letterSpacing: 0 }}>Serving Science Since 1960.</Text>
        	</View>
        );
    }
}