import React from 'react';
import {View, Image, Text, StyleSheet, Modal, TouchableOpacity} from 'react-native';

export default class ModalConfirmMessage extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        const m_instance = {
            alertStatus: this.props.modalStatus,
            alertMessage: this.props.modalMesssage
        };

        this.setState(m_instance);

        return (
            <Modal
                transparent={ true }
                //onOrientationChange='overFullScreen'
                visible={this.state.alertStatus}
                onRequestClose={ () => console.warn('this is') }
            >
                <View style={{ 
                    flex: 1, 
                    backgroundColor: 'rgba(52, 52, 52, 0.8)'
                }}>
                    <View style={{ flexGrow:1, flexDirection: 'column', alignItems: 'center',  marginVertical: 200  }}>
                        <View style={{  backgroundColor: 'black', width:300, height: 180, borderRadius:15 }}>
                            <TouchableOpacity style={{  marginRight:10, alignSelf: 'flex-end' }} onPress={ () => {
                                    this.setState({
                                        alertStatus: false
                                    })
                                }}>
                                    <Text style={{ marginTop:10, width:20, height:20, borderRadius:20, fontWeight: 'bold', textAlign: 'center', backgroundColor:'white', color: 'black' }}>x</Text>
                            </TouchableOpacity>
                            <Text style={{ marginTop: 50, textAlign:'center', fontWeight: 'bold', color: 'white', fontSize: 14 }}>{ this.state.alertMessage }</Text>
                        </View>
                    </View>
                    
                </View>
            </Modal>
        );
    }
}