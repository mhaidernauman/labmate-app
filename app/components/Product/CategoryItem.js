import React from 'react';
import {View, Image, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Config from './../../Services/Config';

import * as Animatable from 'react-native-animatable';

export default class CategoryItem extends React.Component
{
    render()
    {
        let navigate            = this.props.navigation;

        let item_category       = this.props.row;
        let item_category_last  = (item_category[1]);

        return (
            <View style={[styles.bodyContainer, { marginTop: -5}]}>

                <Animatable.View animation="flipInX" style={{  alignContent: 'center', alignItems: 'center',  flexDirection: 'row', width:150, marginTop: 25 }}>
                    <TouchableOpacity onPress={ () => navigate('ProductList', { category_id: item_category[0].id}) } >
                        <Image 
                            source={{uri: Config.base_url() + item_category[0].image_chunks.image_url }}
                            style={{ width:90,  height:90, alignItems: 'center', alignContent: 'center' }}
                        />
                        <Text style={{ textAlign:'center', fontSize:15, marginTop:10, width: 90, height: 40}} >
                            { item_category[0].title }
                        </Text>
                    </TouchableOpacity>
                </Animatable.View>

                {
                (( item_category_last )
                ?
                (<Animatable.View animation="flipInX" style={{  alignContent: 'center', alignItems: 'center', flexDirection: 'row', width:140 }}>
                    <TouchableOpacity onPress={ () => navigate('ProductList', { category_id: item_category[1].id}) } >
                        <Image 
                            source={{uri: Config.base_url() + item_category[1].image_chunks.image_url }}
                            style={{ width:90,  height:90, alignItems: 'center', alignContent: 'center', marginTop: 25 }}
                        />
                        <Text style={{ textAlign:'center', fontSize:15, marginTop:10, width: 90, height: 40}} >
                            { item_category[1].title }
                        </Text>
                    </TouchableOpacity>
                </Animatable.View>)
                :
                null
                )
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({

    bodyContainer: {
        flexDirection:'row',
        marginTop: 30,
        justifyContent:'center',
        alignItems: 'center',
        marginLeft: 50
    },
    
    boxes:{
        padding:10, 
    }
});