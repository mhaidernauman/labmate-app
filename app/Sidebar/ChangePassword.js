import React from 'react';
import { Text, View,  Image, ImageBackground,  StyleSheet, TextInput, TouchableOpacity,  Modal, AsyncStorage} from 'react-native';
import { Header, Content, Footer, Container, Left, Body, Right, Title, Button} from 'native-base';

import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/FontAwesome';

import * as Animatable from 'react-native-animatable';

import Config from './../Services/Config';
import Validation from './../Services/Validation';

import customStyles from './../styles/Custom';

import FooterCopyRight from './../components/Settings/FooterCopyRight';

export default class ChangePassword extends React.Component {

  	constructor( props )
	{
		super( props );

		this.btnFormSubmit = this.btnFormSubmit.bind(this);

		this.state = {

			user_id: 0,

			validate_of_current_password: true,
			validate_of_current_password_msg: '',
			current_password: '',

			validate_of_new_password: true,
			validate_of_new_password_msg: '',
			new_password: '',

			validate_of_confirm_password: true,
			validate_of_confirm_password_msg: '',
			confirm_password: '',

			isLoading: false,

			isMessageModal: false,
			isMessageModalText: ''
		};

		AsyncStorage.getItem('user_id').then((value) => this.setState({ 'user_id': value }));
	};

	_form_validation = () => {

		let current_password 	= this.state.current_password;
		let new_password 		= this.state.new_password;
		let confirm_password 	= this.state.confirm_password;
		let result 				= true;
			
		if( current_password != "") {
			this.setState({ validate_of_current_password: true });
			this.setState({ validate_of_current_password_msg: '' });
		} else {
			this.setState({ validate_of_current_password: false });
			this.setState({ validate_of_current_password_msg: 'This field is required' });
			result = false;
		}

		if( new_password != "" )
		{
			if( new_password.length < 9 ){
				this.setState({ validate_of_new_password: false });
				this.setState({ validate_of_new_password_msg: 'Password must contain must be 8-16 characters long' });
				result = false;
			}

			else if( !Validation.isPassword(new_password) ){
				this.setState({ validate_of_new_password: false }); 
				this.setState({ validate_of_new_password_msg: 'Password must contain alpha-numeric' });
				result = false;
			}

			else {
				this.setState({ validate_of_new_password: true }); 
				this.setState({ validate_of_new_password_msg: '' });
			}
		}
		else
		{
			this.setState({ validate_of_new_password: false });
			this.setState({ validate_of_new_password_msg: 'This field is required' });
			result = false;
		}

		if( confirm_password != "") {
			this.setState({ validate_of_confirm_password: true });
			this.setState({ validate_of_confirm_password_msg: '' });
		} else {
			this.setState({ validate_of_confirm_password: false });
			this.setState({ validate_of_confirm_password_msg: 'This field is required' });
			result = false;
		}
		
		return result;
	}

	btnFormSubmit = ( navigate ) => {
		
		let is_validate = this._form_validation();
		let is_confirm 	= (this.state.new_password == this.state.confirm_password );

		if( is_validate )
		{	
			this.setState({ validate_of_confirm_password: true, validate_of_confirm_password_msg: '' });

			if( is_confirm )
			{
				let postdata = {
					user_id: this.state.user_id,
					old_password: this.state.current_password,
					new_password: this.state.new_password
				}

				// console.warn( postdata );
				this._ajaxPostFormSubmit( postdata, navigate );
			}
			else
			{
				this.setState({ validate_of_confirm_password: false, validate_of_confirm_password_msg: 'Password do not match', confirm_password: '' });
			}
		}
	};

	_ajaxPostFormSubmit( postdata, navigate )
	{
		this.setState({ isLoading: true });

		fetch( Config.base_url() + "/api/user/change-password", {
			method: "POST",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(postdata)
		})
		.then((response) => response.json() )
		.then((res) => {

			this.setState({ isLoading: false });

			if( res.status )
			{
				this.setState({
					current_password: '',
					new_password: '',
					confirm_password: ''
				});

				setTimeout( () => {						
					navigate("ProfileEdit");					
				}, 1500);
			}

			this.setState({
				isMessageModal: true,
				isMessageModalText: res.message
			});
		})
		.catch((err) => console.warn(err) )
		.done();
	}

  render()
  {
	const { navigate } = this.props.navigation;

    return (
		<ImageBackground source={require('./../img/common-background.png')} style={{ flex: 1 }} >

			<Spinner visible={this.state.isLoading} textStyle={{color: '#FFF' }} />
		
			<Container style={{ backgroundColor:'transparent'}}>
				<Header style={{ backgroundColor:'#ed3134' }} >
					<Left>
						<TouchableOpacity onPress={() => this.props.navigation.navigate('DrawerOpen')}>
							<Image 
								source={ require('./../img/icon/menu.png') }
								style={{ height:20, width:20 }}
							/>
						</TouchableOpacity>
					</Left>
					<Body>
						<Title style={{ color:'white', textAlign:'center', alignContent:'center', alignItems:'center', width:206, fontSize:16 }} >Change Password</Title>
					</Body>
					<Right>
	                <TouchableOpacity>
	                  <Button transparent onPress={ () => navigate('ProfileEdit') }>
	                      <Image 
	                      source={ require('./../img/icon/arrow.png') }
	                      style={{ height:12, width:22 }}
	                    />
	                  </Button>
	                </TouchableOpacity>
	              </Right>
				</Header>
				<Content contentContainerStyle={ styles.bodyContainer }>

					<View style={styles.bodySubContainer}>

						<Animatable.View animation="flipInX">
							<TextInput 
								style={ [styles.inputField, !this.state.validate_of_current_password ? styles.err:null] } 
								underlineColorAndroid='transparent'
								autoCapitalize="words"
								secureTextEntry={true}
								placeholder= 'Current Password'
								placeholderTextColor= '#ffffff'
								// value={ this.state.current_password }
								onChangeText = { (text) => this.setState({ current_password: text }) }
								minLength={8}
								maxLength={16}
							/>
						</Animatable.View>
						<View>
							<Text style={ !this.state.validate_of_current_password ? customStyles.usama:null }>{ this.state.validate_of_current_password_msg }</Text>
						</View>

						<Animatable.View animation="flipInX">						
							<TextInput 
								style={ [styles.inputField, !this.state.validate_of_new_password ? styles.err:null] } 
								underlineColorAndroid='transparent'
								autoCapitalize="words"
								secureTextEntry={true}
								placeholder= 'New Password'
								placeholderTextColor= '#ffffff'
								// value={ this.state.new_password }
								onChangeText = { (text) => this.setState({ new_password: text }) }
								minLength={8}
								maxLength={16}
							/>
						</Animatable.View>
						<View>
							<Text style={ !this.state.validate_of_new_password ? customStyles.usama:null }>{ this.state.validate_of_new_password_msg }</Text>
						</View>

						<Animatable.View animation="flipInX">
							<TextInput 
								style={ [styles.inputField, !this.state.validate_of_confirm_password ? styles.err:null] } 
								underlineColorAndroid='transparent'
								autoCapitalize="words"
								secureTextEntry={true}
								placeholder= 'Confirm Password'
								placeholderTextColor= '#ffffff'
								// value={ this.state.confirm_password }
								onChangeText = { (text) => this.setState({ confirm_password: text }) }
								minLength={8}
								maxLength={16}
							/>
						</Animatable.View>
						<View>
							<Text style={ !this.state.validate_of_confirm_password ? customStyles.usama:null }>{ this.state.validate_of_confirm_password_msg }</Text>
						</View>

					</View>

					<Animatable.View animation="flipInX" style={{  flex: 1, alignContent:'center', alignItems: 'center', marginTop: 35 }}>
						<TouchableOpacity 
							style={styles.touchBtnSubmit}
							onPress = {() => this.btnFormSubmit( navigate )}
						>
							<Text style={styles.btnButtonSubmit}>Change Password</Text>
						</TouchableOpacity>
					</Animatable.View>
				</Content>

				<Footer style={{ backgroundColor:'transparent' }}>
					<FooterCopyRight />
				</Footer>

				<Modal
					transparent={ true }
					visible={this.state.isMessageModal}
					onRequestClose={ () => console.warn('this is') }
				>
					<View style={ customStyles.modalBody }>
						<View style={ customStyles.modalContainer }>
							<View style={ customStyles.modalBox }>
								<TouchableOpacity style={ customStyles.closeModalContainer } onPress={ () => {
										this.setState({
											isMessageModal: false
										})
									}}>
										<Icon name="times-circle" size={18} color="white" />
								</TouchableOpacity>
								<Text style={ customStyles.modalTextMessage }>
									{ this.state.isMessageModalText }
								</Text>
							</View>
						</View>
					</View>
				</Modal>

			</Container>
	  </ImageBackground>
    );
  } 
}

const styles = StyleSheet.create({

	bodyContainer: {
		// flex:1,
		alignItems:'center',
		justifyContent: 'center'
	},

	bodySubContainer:{
		flex:1,
		flexDirection: 'column',
		alignItems:'center',
		justifyContent: 'center',
		padding:20
	},

	inputField: {
		width: 300,
		backgroundColor: 'rgba(152, 152, 148, 0.7)',
		borderRadius: 25,
		paddingVertical:10,
		paddingHorizontal:20,
		fontSize: 15,
		color: '#ffffff',
		marginVertical: 5
	},

	touchBtnSubmit: {
		width: 300,
		backgroundColor: '#ed3134',
		borderRadius: 25,
		marginVertical: 10,
		paddingVertical: 12
	},

	btnButtonSubmit: {
		fontSize: 16,
		fontWeight: '500',
		color:'#ffffff',
		textAlign: 'center'
	},

	err: {
		borderWidth:1,
		borderColor: 'red'
	}
});