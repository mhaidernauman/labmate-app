import React from 'react';
import {StyleSheet, Text, View, ScrollView, TextInput, Image, TouchableOpacity, ImageBackground, KeyboardAvoidingView} from 'react-native';

export default class Brands extends React.Component
{
    render()
    {
		return(
			<KeyboardAvoidingView  behavior="padding" enabled style={{
				flex: 1
			  }}>
			<ImageBackground source={require('./../img/common-background.png')} style={{ flex: 1 }} >
				
				<View style={styles.BodyContainer}>

					<View style={styles.headerContainer}>
						<Text style={styles.header}>Brands</Text>
					</View>

					<ScrollView style={{ marginBottom:20 }}>

						<View style={styles.PromotionContainer}>
							
							<View style={styles.boxes}>

								<Text style={styles.boxContent}>Brand 01</Text>
							
							</View>

							<View style={styles.boxes}>
								
								<Text style={styles.boxContent}>Brand 02</Text>
							
							</View>

						</View>

						<View style={styles.PromotionContainer}>
							
							<View style={styles.boxes}>

								<Text style={styles.boxContent}>Brand 03</Text>
							
							</View>

							<View style={styles.boxes}>
								
								<Text style={styles.boxContent}>Brand 04</Text>
							
							</View>

						</View>

						<View style={styles.PromotionContainer}>
							
							<View style={styles.boxes}>

								<Text style={styles.boxContent}>Brand 05</Text>
							
							</View>

							<View style={styles.boxes}>
								
								<Text style={styles.boxContent}>Brand 06</Text>
							
							</View>

						</View>


						<View style={styles.PromotionContainer}>
							
							<View style={styles.boxes}>
								<Text style={styles.boxContent}>Brand 07</Text>
							</View>

							<View style={styles.boxes}>
								<Text style={styles.boxContent}>Brand 08</Text>
							</View>

						</View>

					</ScrollView>	
				</View>
				
			</ImageBackground>
			</KeyboardAvoidingView>
		);
	};
}

const styles = StyleSheet.create({

	BodyContainer: {		
		flex: 1,
	},

	headerContainer:{
		backgroundColor:'#ed3134',
		alignItems: 'center',
		justifyContent:'center',
	},

	header:{
		padding:20,
		color:'white',
	},

	PromotionContainer:{
		flexDirection:'row',
		margin:5,
		justifyContent:'center',
	},

	boxes:{
		width:150, 
		height:150, 
		margin:5, 
		borderRadius:5, 
		backgroundColor:'#5a5a58'
	},

	boxContent:{
		justifyContent:'center', 
		alignItems: 'center', 
		fontSize:12, 
		color:'#fff',
		marginTop:65,
		marginLeft:54
	}
	
});