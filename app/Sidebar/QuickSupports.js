import React from 'react';
import { Text, View, Image, ImageBackground, StyleSheet, TouchableOpacity } from 'react-native';
import { Header, Content, Footer, Container, Left, Body, Right, Title, Button} from 'native-base';

import * as Animatable from 'react-native-animatable';

import FooterCopyRight from './../components/Settings/FooterCopyRight';

export default class QuickSupports extends React.Component {
  
  static navigationOptions = {
    drawerIcon: (
      <Image 
        source={ require('./../img/icon/gender-icon.png') }
        style={{ height:20, width:20 }}
      />
    )
  }

  render()
  {
    const { navigate } = this.props.navigation;
    return (
      <ImageBackground source={require('./../img/common-background.png')} style={{ flex: 1 }} >
      
        <Container style={{ backgroundColor:'transparent'}}>
          <Header style={{ backgroundColor:'#ed3134' }} >
            <Left>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate('DrawerOpen')}>
              <Image 
                source={ require('./../img/icon/menu.png') }
                style={{ height:20, width:20 }}
              />
            </TouchableOpacity>
          </Left>
          <Body>
            <Title style={{ color:'white', textAlign:'center', alignContent:'center', alignItems:'center', width:206, fontSize:16 }} >Support Hub</Title>
          </Body>
          <Right>
            <TouchableOpacity>
              <Button transparent onPress={() => navigate('Home') }>
                  <Image 
                  source={ require('./../img/icon/arrow.png') }
                  style={{ height:12, width:22 }}
                />
              </Button>
            </TouchableOpacity>
          </Right>
          </Header>
          <Content contentContainerStyle={ styles.bodyContainer }>

            <View style={{ flex:1, flexDirection: 'column' }}>

              <Animatable.View animation="flipInX">
                <TouchableOpacity onPress={ () => navigate('GetAnAppoinment') }>
                  <View style={{ width:250, marginTop:10, padding: 30, paddingBottom: 7, borderWidth: 1, borderColor: '#868686', backgroundColor: 'rgba(250, 254, 250, 0.5)' }}>
                    <View style={{ alignContent: 'center', alignItems: 'center' }}>
                      <Image source={require('./../img/icon/appointment-icon.png')} style={{ width: 40, height: 40 }} />
                    </View>
                    <View style={{ borderBottomColor: '#ed3134', borderBottomWidth: 2, width: 80, marginTop: 10, marginLeft: 51, alignItems: 'center' }} ></View>
                    <Text style={{ color: '#797979', fontWeight:'bold', fontSize: 14, paddingTop: 10, textAlign: 'center', marginBottom: 20 }}>Schedule a Meeting</Text>
                  </View>
                </TouchableOpacity>
              </Animatable.View>

              <Animatable.View animation="flipInX">
                <TouchableOpacity onPress={ () => navigate('GetEnquiry') }>
                  <View style={{ marginTop:10, padding: 30, paddingBottom: 7, borderWidth: 1, borderColor: '#868686', backgroundColor: 'rgba(250, 254, 250, 0.5)'  }}>
                    <View style={{ alignContent: 'center', alignItems: 'center' }}>
                      <Image source={require('./../img/icon/techincal-support-icon.png')} style={{ width: 35, height: 40 }} />
                    </View>
                    <View style={{ borderBottomColor: '#ed3134', borderBottomWidth: 2, width: 80, marginTop: 10, marginLeft: 55, alignItems: 'center' }} ></View>
                    <Text style={{ color: '#797979', fontWeight:'bold', fontSize: 14, paddingTop: 10, textAlign: 'center', marginBottom: 20 }}>Technical Support</Text>
                  </View>
                </TouchableOpacity>
              </Animatable.View>

              <Animatable.View animation="flipInX">
                <TouchableOpacity onPress={ () => navigate('CertificateOfAnalysis') }>
                  <View style={{ marginTop:10, padding: 30, paddingBottom: 7, borderWidth: 1, borderColor: '#868686', backgroundColor: 'rgba(250, 254, 250, 0.5)'  }}>
                    <View style={{ alignContent: 'center', alignItems: 'center' }}>
                      <Image source={require('./../img/icon/certificate-of-analysis-icon.png')} style={{ width: 35, height: 40 }} />
                    </View>
                    <View style={{ borderBottomColor: '#ed3134', borderBottomWidth: 2, width: 80, marginTop: 10, marginLeft: 55, alignItems: 'center' }} ></View>
                    <Text style={{ color: '#797979', fontWeight:'bold', fontSize: 14, paddingTop: 10, textAlign: 'center', marginBottom: 20 }}>Certificate of Analysis</Text>
                  </View>
                </TouchableOpacity>
              </Animatable.View>

            </View>

          </Content>
          <Footer style={{ backgroundColor:'transparent' }}>
            <FooterCopyRight />
          </Footer>
        </Container>
      </ImageBackground>
    );
  } 
}

const styles = StyleSheet.create({

  bodyContainer: {
    flex:1,
    alignItems:'center',
    justifyContent: 'center'
  }
});