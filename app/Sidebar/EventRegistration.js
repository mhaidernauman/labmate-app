import React from 'react';
import { Text, View,  Image, ImageBackground,  StyleSheet, TextInput, TouchableOpacity,  Modal, AsyncStorage, KeyboardAvoidingView} from 'react-native';
import { Header, Content, Footer, Container, Left, Body, Right, Title, Button} from 'native-base';

import DatePicker from 'react-native-datepicker';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/FontAwesome';
import FCM, {FCMEvent} from "react-native-fcm";

import * as Animatable from 'react-native-animatable';

import Config from './../Services/Config';
import Validation from './../Services/Validation';

import customStyles from './../styles/Custom';

import FooterCopyRight from './../components/Settings/FooterCopyRight';

export default class EventRegistration extends React.Component {

  	constructor( props )
	{
		super( props );

		this.btnFormSubmit = this.btnFormSubmit.bind(this);

		this.state = {

			event_id : 0,
			user_id: 0,

			validate_of_name: true,
			validate_of_name_msg: '',
			name: '',

			validate_of_email: true,
			validate_of_email_msg: '',
			email: '',

			validate_of_phone: true,
			validate_of_phone_msg: '',
			phone: '',

			validate_of_program: true,
			validate_of_program_msg: '',
			program: '',

			validate_of_time_slot: true,
			validate_of_time_slot_msg: '',
			time_slot: '',

			validate_of_message: true,
			validate_of_message_msg: '',
			message: '',

			isLoading: false,

			isMessageModal: false,
			isMessageModalText: ''
		};

		AsyncStorage.getItem('user_id').then((value) => this.setState({ 'user_id': value }));

		FCM.on(FCMEvent.Notification, (notif) => {
           this.setState({ isMessageModal: true, isMessageModalText: notif.body })
        });
	};

	_form_validation = () => {

		let name 		= this.state.name;
		let email 		= this.state.email;
		let phone 		= this.state.phone;
		let program 	= this.state.program;
		let time_slot 	= this.state.time_slot;
		let message 	= this.state.message;

		this.setState({ validate_of_name: ((name != "" && Validation.isStringAndSpeace(name) ) ? true : false) });
		this.setState({ validate_of_name_msg: (
			(name == "" ) ? 
			'This field is required' : 
			(!Validation.isStringAndSpeace(name) ? 'Only letters are allowed' : '')
		) });

		this.setState({ validate_of_phone: ((phone != "" && Validation.isNumber(phone) ) ? true : false) });
		this.setState({ validate_of_phone_msg: (
			(phone == "" ) ? 
			'This field is required' : 
			(!Validation.isNumber(phone) ? 'This format is invalid' : '')
		) });

		this.setState({ validate_of_email: ((email != "" && Validation.isEmail(email) ) ? true : false) });
		this.setState({ validate_of_email_msg: (
			(email == "" ) ? 
			'This field is required' : 
			(!Validation.isEmail(email) ? 'Email format should be joe@example.com' : '')
		) });

		this.setState({ validate_of_program: ((program != "" && !Validation.isNotAllowSpecialCharacters(program) ) ? true : false) });
		this.setState({ validate_of_program_msg: (
			(program == "" ) ? 
			'This field is required' : 
			(Validation.isNotAllowSpecialCharacters(program) ? 'Mobile format should be numeric' : '')
		) });

		this.setState({ validate_of_time_slot: ( time_slot != "" ? true : false) });
		this.setState({ validate_of_time_slot_msg: ( time_slot == "" ? 'This field is required' : '') });

		this.setState({ validate_of_message: ((message != "" && !Validation.isNotAllowSpecialCharacters(message) ) ? true : false) });
		this.setState({ validate_of_message_msg: (
			(message == "" ) ? 
			'This field is required' : 
			(Validation.isNotAllowSpecialCharacters(message) ? 'This format is invalid' : '')
		) });
	}

	btnFormSubmit = () => {
		
		this._form_validation();

		let is_validate = ( 
			(this.state.validate_of_name && this.state.name ) &&
			( this.state.validate_of_email && this.state.email) &&
			(this.state.validate_of_phone && this.state.phone) &&
			(this.state.validate_of_program && this.state.program) &&
			(this.state.validate_of_time_slot && this.state.time_slot) &&
			(this.state.validate_of_message && this.state.message)
		);
		
		if( is_validate )
		{
			let postdata = {
				event_id: this.state.event_id,
				user_id: this.state.user_id,
				name: this.state.name,
				email: this.state.email,
				phone: this.state.phone,
				program: this.state.program,
				time_slot: this.state.time_slot,
				message: this.state.message
			}

			this._ajaxPostFormSubmit( postdata );
		}
	};

	_ajaxPostFormSubmit( postdata )
	{
		this.setState({ isLoading: true });

		fetch( Config.base_url() + "/api/user/event-registration", {
			method: "POST",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(postdata)
		})
		.then((response) => response.json() )
		.then((res) => {

			this.setState({ isLoading: false });

			if( res.status )
			{
				this.setState({
					name: '',
					email: '',
					phone: '',
					program: '',
					time_slot: '',
					message: ''
				});
			}

			this.setState({
				isMessageModal: true,
				isMessageModalText: res.message
			});
		})
		.catch(function(error){
			console.warn(error);
		})
		.done();
	}

	componentDidMount()
	{
		const {params} = this.props.navigation.state;
		
		this.setState({
			event_id: params.event_id
		});
	};

  render()
  {
	const { navigate } = this.props.navigation;

    return (

		<KeyboardAvoidingView  behavior="padding" enabled style={{
			flex: 1
		  }}>
		<ImageBackground source={require('./../img/common-background.png')} style={{ flex: 1 }} >

			<Spinner visible={this.state.isLoading} textStyle={{color: '#FFF' }} />
		
			<Container style={{ backgroundColor:'transparent'}}>
				<Header style={{ backgroundColor:'#ed3134' }} >
					<Left>
						<TouchableOpacity onPress={() => this.props.navigation.navigate('DrawerOpen')}>
							<Image 
								source={ require('./../img/icon/menu.png') }
								style={{ height:20, width:20 }}
							/>
						</TouchableOpacity>
					</Left>
					<Body>
						<Title style={{ color:'white', textAlign:'center', alignContent:'center', alignItems:'center', width:206, fontSize:16 }} >Event Registration</Title>
					</Body>
					<Right>
	                <TouchableOpacity>
	                  <Button transparent onPress={ () => navigate('EventDetail', { event_id: this.state.event_id }) }>
	                      <Image 
	                      source={ require('./../img/icon/arrow.png') }
	                      style={{ height:12, width:22 }}
	                    />
	                  </Button>
	                </TouchableOpacity>
	              </Right>
				</Header>
				<Content contentContainerStyle={ styles.bodyContainer }>

					<View style={styles.bodySubContainer}>

							<Animatable.View animation="flipInX">
								<TextInput 
									style={ [styles.inputField, !this.state.validate_of_name ? styles.err:null] } 
									underlineColorAndroid='transparent'
									placeholder= 'Full Name'
									placeholderTextColor= '#ffffff'
									// value={ this.state.name }
									onChangeText = { (text) => this.setState({ name: text }) }
									minLength={5}
									maxLength={20}
								/>
							</Animatable.View>
							<View>
								<Text style={ !this.state.validate_of_name ? customStyles.usama:null }>{ this.state.validate_of_name_msg }</Text>
							</View>

							<Animatable.View animation="flipInX">						
								<TextInput 
									style={ [styles.inputField, !this.state.validate_of_email ? styles.err:null] } 
									underlineColorAndroid='transparent'
									placeholder= 'Email'
									keyboardType="email-address"
									placeholderTextColor= '#ffffff'
									// value={ this.state.email }
									onChangeText = { (text) => this.setState({ email: text }) }
									maxLength={40}
								/>
							</Animatable.View>
							<View>
								<Text style={ !this.state.validate_of_email ? customStyles.usama:null }>{ this.state.validate_of_email_msg }</Text>
							</View>

							<Animatable.View animation="flipInX">
								<TextInput 
									style={ [styles.inputField, !this.state.validate_of_phone ? styles.err:null] } 
									underlineColorAndroid='transparent'
									placeholder= 'Phone'
									keyboardType = 'numeric'
									placeholderTextColor= '#ffffff'
									// value={ this.state.phone }
									onChangeText = { (text) => this.setState({ phone: text }) }
									minLength={11}
									maxLength={20}
								/>
							</Animatable.View>

							<View>
								<Text style={ !this.state.validate_of_phone ? customStyles.usama:null }>{ this.state.validate_of_phone_msg }</Text>
							</View>

							<Animatable.View animation="flipInX">
								<TextInput 
									style={ [styles.inputField, !this.state.validate_of_program ? styles.err:null] } 
									underlineColorAndroid='transparent'
									placeholder= 'Program'
									placeholderTextColor= '#ffffff'
									// value={ this.state.program }
									onChangeText = { (text) => this.setState({ program: text }) }
									minLength={5}
									maxLength={100}
								/>
							</Animatable.View>
							<View>
								<Text style={ !this.state.validate_of_program ? customStyles.usama:null }>{ this.state.validate_of_program_msg }</Text>
							</View>

							<Animatable.View animation="flipInX">
								<DatePicker
									style={ [styles.inputDateField, !this.state.validate_of_time_slot ? styles.err:null] } 
									date={this.state.time_slot}
									mode="time"
									showIcon={false}
									value={ this.state.time_slot}
									placeholder="Time Slot"
									format="h:mm:ss a"
									confirmBtnText="Confirm"
									cancelBtnText="Cancel"
									customStyles={
										{
											dateIcon: {
												position: 'absolute',
												right: 0,
												top: 4
											},
											dateInput: {
												borderWidth: 0,
												alignItems: 'flex-start',
											},
											placeholderText: {
											fontSize: 16,
											color: '#fff'
											},
											dateText: {
												fontSize: 16,
												color: '#fff'
											}
										}
									}
									onDateChange={(time) => {this.setState({time_slot: time})}}
								/>
							</Animatable.View>
							<View>
								<Text style={ !this.state.validate_of_time_slot ? customStyles.usama:null }>{ this.state.validate_of_time_slot_msg }</Text>
							</View>

							<Animatable.View animation="flipInX">
								<TextInput
									style={ [styles.fieldTextarea, !this.state.validate_of_message ? styles.err:null] } 
									multiline={true}
									numberOfLines={6}
									maxHeight={150}
									minHeight={150}
									underlineColorAndroid='transparent'
									placeholder= 'Message'
									placeholderTextColor= '#ffffff'
									// value={ this.state.message }
									onChangeText = { (text) => this.setState({ message: text }) }
									minLength={10}
									maxLength={250}
								/>								
							</Animatable.View>
							<View>
								<Text style={ !this.state.validate_of_message ? customStyles.usama:null }>{ this.state.validate_of_message_msg }</Text>
							</View>
					</View>

					<Animatable.View animation="flipInX" style={{  flex: 1, alignContent:'center', alignItems: 'center', marginTop: 5 }}>
						<Button 
							style={styles.touchBtnSubmit}
							onPress = {() => this.btnFormSubmit()}
						>
							<Text style={styles.btnButtonSubmit}>Submit</Text>
						</Button>
					</Animatable.View>
				</Content>

				<Footer style={{ backgroundColor:'transparent' }}>
					<FooterCopyRight />
				</Footer>

				<Modal
					transparent={ true }
					visible={this.state.isMessageModal}
					onRequestClose={ () => console.warn('this is') }
				>
					<View style={ customStyles.modalBody }>
						<View style={ customStyles.modalContainer }>
							<View style={ customStyles.modalBox }>
								<TouchableOpacity style={ customStyles.closeModalContainer } onPress={ () => {
										this.setState({
											isMessageModal: false
										})
									}}>
										<Icon name="times-circle" size={18} color="white" />
								</TouchableOpacity>
								<Text style={ customStyles.modalTextMessage }>
									Event Registered Successfully
								</Text>
							</View>
						</View>
					</View>
				</Modal>

			</Container>
	  </ImageBackground>
	  </KeyboardAvoidingView>
    );
  } 
}

const styles = StyleSheet.create({

	bodyContainer: {
		// flex:1,
		alignItems:'center',
		justifyContent: 'center'
	},

	bodySubContainer:{
		flex:1,
		flexDirection: 'column',
		alignItems:'center',
		justifyContent: 'center',
		padding:20
	},

	inputField: {
		width: 270,
		backgroundColor: 'rgba(152, 152, 148, 0.7)',
		borderRadius: 25,
		paddingVertical:10,
		paddingHorizontal:20,
		fontSize: 15,
		color: '#ffffff',
		marginVertical: 5
	},

	fieldTextarea: {
		width: 270,
		textAlignVertical: 'top',
		backgroundColor: 'rgba(152, 152, 148, 0.7)',
		borderRadius: 25,
		fontSize: 16,
		color: '#ffffff',
		paddingHorizontal: 20,
		paddingVertical:12,
		marginVertical: 5
	},

	touchBtnSubmit: {
		width: 270,
		backgroundColor: '#ed3134',
		borderRadius: 25,
		marginVertical: 10,
		paddingVertical: 12
	},

	btnButtonSubmit: {
		width: 270,
		fontSize: 16,
		fontWeight: '500',
		color:'#ffffff',
		textAlign: 'center'
	},

	inputDateField: {
        width: 270,
		backgroundColor: 'rgba(152, 152, 148, 0.7)',
		borderRadius: 25,
		paddingHorizontal: 20,
		paddingVertical:4,
		marginVertical: 5,
	},
	
	err: {
		borderWidth:1,
		borderColor: 'red'
	}
});