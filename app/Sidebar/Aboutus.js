import React from 'react';
import {Text, View, Image, ImageBackground, TouchableOpacity, StyleSheet} from 'react-native';
import { Header, Content, Footer, Container, Left,  Body, Right,  Title, Button} from 'native-base';
import * as Animatable from 'react-native-animatable';
import Spinner from 'react-native-loading-spinner-overlay';

// Components
import Config from './../Services/Config';
import FooterCopyRight from './../components/Settings/FooterCopyRight';

export default class Aboutus extends React.Component {
  
  static navigationOptions = {
    drawerIcon: (
      <Image 
        source={ require('./../img/icon/about-us-icon.png') }
        style={{ height:20, width:20 }}
      />
    )
  }

  constructor()
  {
    super();
    
    this.state = {
      dataSource: null,
      isLoading: false
    };
  };

  componentDidMount()
  {

    this.setState({ isLoading: true });
    
    let api_url = (Config.base_url() + "/api/data/about-us");

    fetch( api_url, {
      method: "GET",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
    .then( (response) => response.json() )
    .then( (res) => {

      this.setState({ isLoading: false });

      if ( res.status )
      {
        this.setState({
          dataSource: res.data
        });
      }

    })
    .catch(error => console.log(error))
    .done();
  };

  render()
  {
    const {navigate} = this.props.navigation;

    let dataSource = this.state.dataSource;

    return (
      <ImageBackground source={require('./../img/common-background.png')} style={{ flex: 1 }} >

        <Spinner visible={this.state.isLoading} textStyle={{color: '#FFF' }} />

        <Container style={{ backgroundColor:'transparent'}}>
          <Header style={{ backgroundColor:'#ed3134' }} >
            <Left>
							<TouchableOpacity onPress={()=>this.props.navigation.navigate('DrawerOpen')}>
								<Image 
									source={ require('./../img/icon/menu.png') }
									style={{ height:20, width:20 }}
								/>
							</TouchableOpacity>
						</Left>
						<Body>
							<Title style={{ color:'white', textAlign:'center', alignContent:'center', alignItems:'center', width:206, fontSize:16 }} >About Us</Title>
						</Body>
						<Right>
              <TouchableOpacity>
                <Button transparent onPress={() => navigate('Home') }>
                    <Image 
                    source={ require('./../img/icon/arrow.png') }
                    style={{ height:12, width:22 }}
                  />
                </Button>
              </TouchableOpacity>
            </Right>
          </Header>
          
          <Content contentContainerStyle={ styles.bodyContainer }>  
            <View style={ [styles.bodySubContainer, { marginTop: 30}] }>
                <Animatable.Text animation="flipInX" style={ styles.heading1 }>
                { ((dataSource && dataSource.block1) ? dataSource.block1.title : null ) }
                </Animatable.Text>
                <Animatable.View animation="flipInX" style={{ borderBottomColor: '#ed3134', borderBottomWidth: 2, width: 90, marginVertical: 15,  alignItems: 'center' }} ></Animatable.View>
                <Animatable.Text animation="flipInX" style={ styles.paragraph }>
                  { ((dataSource && dataSource.block1) ? dataSource.block1.short_description : null ) }
                </Animatable.Text>
            </View>

            <View style={ styles.bodySubContainer }>
                <Animatable.Text animation="flipInX" style={ styles.heading1 }>
                  { ((dataSource && dataSource.block2) ? dataSource.block2.title : null ) }
                </Animatable.Text>
                <Animatable.View animation="flipInX" style={{ borderBottomColor: '#ed3134', borderBottomWidth: 2, width: 90, marginVertical: 15,  alignItems: 'center' }} ></Animatable.View>
                <Animatable.Text animation="flipInX" style={ styles.paragraph }>
                  { ((dataSource && dataSource.block2) ? dataSource.block2.short_description : null ) }
                </Animatable.Text>
            </View>
          </Content>

          <Footer style={{ backgroundColor:'transparent' }}>
            <FooterCopyRight />
          </Footer>

        </Container>
      </ImageBackground>
    );
  } 
}

const styles = StyleSheet.create({

  bodyContainer: {
    flex:1,
    alignItems:'center',
    justifyContent: 'center'
  },

  bodySubContainer:{
    flex:1,
    alignItems:'center',
    justifyContent: 'center',
    padding:20
  },

  heading1:{
    alignItems:'center',
    justifyContent: 'center',
    fontFamily: 'Roboto-Bold',
    fontWeight: 'bold',
    fontSize: 20,
    color: '#ed3135',
    textAlign: 'center',
    marginVertical: 10
  },

  paragraph:{
    alignItems:'center',
    justifyContent: 'center',
    textAlign: 'center',
    fontSize: 14,
    fontFamily: 'Roboto-bold'
  }

});