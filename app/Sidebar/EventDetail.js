import React from 'react';
import {Text, View, Image, ImageBackground, TouchableOpacity} from 'react-native';
import { Header, Content, Footer, Container, Left,  Body, Right,  Title, Button} from 'native-base';
import Spinner from 'react-native-loading-spinner-overlay';

import Config from './../Services/Config';
import Icon from 'react-native-vector-icons/FontAwesome';

import * as Animatable from 'react-native-animatable';

import SingleGrid from './../styles/SingleGridDetail';
import FooterCopyRight from './../components/Settings/FooterCopyRight';

export default class EventDetail extends React.Component {

  	constructor(props)
	{
		super(props);

		this.state = {

			isLoading: false,

			single_detail: null,
			top_title: ''
		};
	};

	getSinglePage( id )
	{
		this.setState({ isLoading: true });

		let api_url = (Config.base_url() + "/api/data/single-event-detail?event_id=" + id );
		
		fetch( api_url, {
			method: "GET",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			}
		})
		.then( (response) => response.json() )
		.then( (res) => {

			this.setState({ isLoading: false });

			let data = res.data;

			this.setState({
				single_detail: data,
				top_title: data.title
			});

		})
		.catch(error => console.log(error))
		.done();
	}

	componentDidMount()
	{
		const {params} = this.props.navigation.state;
		
		this.getSinglePage( params.event_id );
	};

	render()
	{
		const is_event = (this.state.single_detail);
		const { navigate } = this.props.navigation;

		return (
		<ImageBackground source={require('./../img/common-background.png')} style={{ flex: 1 }} >

			<Spinner visible={this.state.isLoading} textStyle={{color: '#FFF' }} />

			<Container style={{ backgroundColor:'transparent'}}>
				<Header style={{ backgroundColor:'#ed3134' }} >
					<Left>
						<TouchableOpacity onPress={() => this.props.navigation.navigate('DrawerOpen')}>
							<Image 
								source={ require('./../img/icon/menu.png') }
								style={{ height:20, width:20 }}
							/>
						</TouchableOpacity>
					</Left>
					<Body>
						<Title style={{ color:'white', textAlign:'center', alignContent:'center', alignItems:'center', width:206, fontSize:16 }} >Events</Title>
					</Body>
					<Right>
	                <TouchableOpacity>
	                  <Button transparent onPress={() => navigate('Events') }>
	                      <Image 
	                      source={ require('./../img/icon/arrow.png') }
	                      style={{ height:12, width:22 }}
	                    />
	                  </Button>
	                </TouchableOpacity>
	              </Right>
				</Header>
				<Content>
					<View style={  SingleGrid.container }>
						{ 
						(( is_event )
						?
						(<View>
							<Animatable.View animation="flipInX" style={ SingleGrid.header }>
								<Image 
									source={{uri: Config.base_url() + this.state.single_detail.image_chunks.image_url }}
									style={{ flex:1, width:"100%", borderRadius: 10, height:200 }}
								/>
								<Image 
			                        source={ require('./../img/shadow.png') }
			                        style={{ height:50, width:"100%", borderBottomLeftRadius: 10, borderRadius:10, borderBottomRightRadius: 10, position:'absolute', top:150 }}
			                    />
								<Text style={{ fontFamily: 'Roboto-Bold', fontSize: 16, marginBottom:10, marginTop:-32, marginLeft:10, color:'white', fontSize:16 }}>{this.state.single_detail.title}</Text>
							</Animatable.View>
							
							<Animatable.View animation="flipInX" style={ SingleGrid.header2 }>
								<View style={{ width:240, flex:1.5, flexDirection: 'row' }}>
									<Image 
										source={ require('./../img/icon/address-icon.png') }
										style={{ height:15, width:12, marginTop: 3 }}
									/>
									<Text style={{ marginLeft: 5, marginTop: 1, fontFamily: 'Roboto-Bold', fontSize: 14 }} >
										{this.state.single_detail.location}
									</Text>
								</View>
								<View style={{ width:150, flex:0.5, flexDirection: 'row' }}>
									<Icon name="clock-o"  style={{ fontSize:18, color:'#696969' }} />
									<Text style={{ fontFamily: 'Roboto-Bold', fontSize: 14,  marginTop: 1 }}>
										{this.state.single_detail.event_time}
									</Text>
								</View>
							</Animatable.View>

							<Animatable.View animation="flipInX" style={ SingleGrid.header3 }>
								<View style={{ flexDirection: 'row' }}>
									<Icon name="calculator" style={{ fontSize:14, color:'#696969', marginTop: 2 }} />
									<Text style={{ fontFamily: 'Roboto-Bold', fontSize: 14, marginLeft: 5, marginTop: 1 }} >
										{this.state.single_detail.is_format.event_date}
									</Text>
								</View>
							</Animatable.View>

							<Animatable.View animation="flipInX" style={{ marginTop: 15 }}>
								<Text style={{ fontFamily: 'Roboto-Bold', fontSize: 18, fontWeight: 'bold', color:'#ed3134' }} >Description</Text>
							</Animatable.View>
							
							<Animatable.View animation="flipInX" style={ SingleGrid.body }>
								<Text style={{ fontFamily: 'Roboto-Bold', fontSize: 14, marginRight:12 }}>
									{this.state.single_detail.description}
								</Text>
							</Animatable.View>

							{ (this.state.single_detail.is_btn_register == 1) ? (
							<Animatable.View animation="flipInX" style={{ alignContent:'center', alignItems: 'center', marginTop: 50}}>
								<TouchableOpacity 
									style={{ width: 300, backgroundColor: '#ed3134', borderRadius: 25, marginVertical: 10, paddingVertical: 12 }}
									
									onPress={ () => navigate('EventRegistration', { event_id: this.state.single_detail.id }) }
								>
									<Text style={{ fontSize: 16, fontWeight: '500', color:'#ffffff', textAlign: 'center' }}>Register Event</Text>
								</TouchableOpacity>
							</Animatable.View>) : 
							null }

						</View>)
						: 
						null
						)
						}
					</View>
				</Content>
				<Footer style={{ backgroundColor:'transparent' }}>
					<FooterCopyRight />
				</Footer>
			</Container>
		</ImageBackground>
		);
	} 
}