import React from 'react';
import {Text, View, Image, ImageBackground, TouchableOpacity} from 'react-native';
import { Header, Content, Footer, Container, Left,  Body, Right,  Title, Button } from 'native-base';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/FontAwesome';

import * as Animatable from 'react-native-animatable';

import Config from './../Services/Config';

import SingleGrid from './../styles/SingleGridDetail';
import FooterCopyRight from './../components/Settings/FooterCopyRight';

export default class PromotionDetail extends React.Component {

  	constructor(props)
	{
		super(props);

		this.state = {
			
			isLoading: false,

			page_title: '',
			page_link: 'newsroom',

			single_detail: null
		};
	};

	getSinglePage( id )
	{
		this.setState({ isLoading: true });

		let api_url = (Config.base_url() + "/api/data/single-promotion-detail?promotion_id=" + id );
		
		fetch( api_url, {
			method: "GET",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			}
		})
		.then( (response) => response.json() )
		.then( (res) => {

			this.setState({ isLoading: false });
			
			let data = res.data;
			
			this.setState({
				page_title: data.title,
				page_link: ((data.type == 'product-news') ? 'ProductNews' : 'ResearchNews'),
				single_detail: data
			});

		})
		.catch(error => console.log(error))
		.done();
	}

	componentDidMount()
	{
		const {params} = this.props.navigation.state;
		
		// console.warn(params);

		this.getSinglePage( params.promotion_id );
	};

  render()
  {
	const is_promotion = (this.state.single_detail);
	const { navigate } = this.props.navigation;

    return (
	<ImageBackground source={require('./../img/common-background.png')} style={{ flex: 1 }} >

		<Spinner visible={this.state.isLoading} textStyle={{color: '#FFF' }} />

		<Container style={{ backgroundColor:'transparent'}}>
			<Header style={{ backgroundColor:'#ed3134' }} >
				<Left>
					<TouchableOpacity onPress={()=>this.props.navigation.navigate('DrawerOpen')}>
						<Image 
							source={ require('./../img/icon/menu.png') }
							style={{ height:20, width:20 }}
						/>
					</TouchableOpacity>
				</Left>
				<Body>
					<Title style={{ color:'white', textAlign:'center', alignContent:'center', alignItems:'center', width:206, fontSize:16 }} >
						{ this.state.page_title }
					</Title>
				</Body>
				<Right>
	                <TouchableOpacity>
	                  <Button transparent onPress={() => navigate( this.state.page_link ) }>
	                      <Image 
	                      source={ require('./../img/icon/arrow.png') }
	                      style={{ height:12, width:22 }}
	                    />
	                  </Button>
	                </TouchableOpacity>
	              </Right>
			</Header>
			<Content>
				<View style={  SingleGrid.container }>
					{ 
					(( is_promotion )
					?
					(<View>
						<Animatable.View animation="flipInX" style={{ marginTop: 15 }}>
							<Image 
								source={{uri: Config.base_url() + this.state.single_detail.image_chunks.image_url }}
								style={{ flex:1, width:"100%", borderRadius: 10, height:200 }}
							/>
							<Image 
		                        source={ require('./../img/shadow.png') }
		                        style={{ height:50, width:"100%", borderBottomLeftRadius: 10, borderBottomRightRadius: 10, borderRadius:5, position:'absolute', top:150 }}
		                    />
							<Text style={{ fontFamily: 'Roboto-Bold', marginBottom:20, fontSize: 16, marginTop:-32, marginLeft:10, color:'white', fontSize:16 }}>{this.state.single_detail.title}</Text>
						</Animatable.View>

						<Animatable.View animation="flipInX" style={{ marginTop: 10 }}>
							<Text style={{ fontFamily: 'Roboto-Bold', fontSize: 18, fontWeight: 'bold', color:'#ed3134' }} >Description</Text>
						</Animatable.View>
						
						<Animatable.View animation="flipInX" style={ SingleGrid.body }>
							<Text style={{ fontFamily: 'Roboto-Bold', fontSize: 14, marginRight:18 }}>
								{this.state.single_detail.description}
							</Text>
						</Animatable.View>

					</View>)
					: 
					null
					)
					}
				</View>
			</Content>
			<Footer style={{ backgroundColor:'transparent' }}>
				<FooterCopyRight />
			</Footer>
		</Container>
	  </ImageBackground>
    );
  } 
}