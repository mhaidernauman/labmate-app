import React from 'react';
import { Text, View,  Image, ImageBackground,  StyleSheet, TextInput, TouchableOpacity, AsyncStorage, Modal } from 'react-native';
import { Header, Content, Footer, Container, Left, Body, Right, Title, Button} from 'native-base';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/FontAwesome';
import FCM, {FCMEvent} from "react-native-fcm";

import * as Animatable from 'react-native-animatable';

import Config from './../Services/Config';
import Validation from './../Services/Validation';

import FooterCopyRight from './../components/Settings/FooterCopyRight';
import customStyles from './../styles/Custom';

export default class GetEnquiry extends React.Component {

  	constructor( props )
	{
		super( props );

		this.btnFormSubmit = this.btnFormSubmit.bind(this);

		this.state = {

			user_id: 0,

			validate_of_message: true,
			validate_of_message_msg: '',
			message: '',

			isLoading: false,
			isMessageModal: false,
			isMessageModalText: ''
		};

		AsyncStorage.getItem('user_id').then((value) => this.setState({ 'user_id': value }));

		FCM.on(FCMEvent.Notification, (notif) => {
           this.setState({ isMessageModal: true, isMessageModalText: notif.body })
        });
	};

	_form_validation = () => {

		let message 	= this.state.message;

		this.setState({ validate_of_message: ((message != "" && !Validation.isNotAllowSpecialCharacters(message) ) ? true : false) });
		this.setState({ validate_of_message_msg: (
			(message == "" ) ? 
			'This field is required' : 
			(Validation.isNotAllowSpecialCharacters(message) ? 'This format is invalid' : '')
		) });
	}

	btnFormSubmit = () => {
		
		this._form_validation();

		let is_validate = ( this.state.validate_of_message && this.state.message );
		
		if( is_validate )
		{
			let postdata = {
				user_id: this.state.user_id,
				message: this.state.message
			}

			this.ajaxPostFormSubmit( postdata );
		}
	};

	ajaxPostFormSubmit( postdata )
	{
		this.setState({ isLoading: true });

		fetch( Config.base_url() + "/api/user/book-enquiry", {
			method: "POST",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(postdata)
		})
		.then((response) => response.json() )
		.then((res) => {

			this.setState({ isLoading: false });

			if( res.status )
			{	
				this.setState({
					message: ''
				});
			}

			this.setState({ 
				isMessageModal: true,
				isMessageModalText: res.message
			});
		})
		.catch(function(error){
			console.warn(error);
		})
		.done();
	}

  render()
  {
	const {goBack} = this.props.navigation;

    return (
		<ImageBackground source={require('./../img/common-background.png')} style={{ flex: 1 }} >

			<Spinner visible={this.state.isLoading} textStyle={{color: '#FFF' }} />

			<Container style={{ backgroundColor:'transparent'}}>
				<Header style={{ backgroundColor:'#ed3134' }} >
					<Left>
						<TouchableOpacity onPress={ () => this.props.navigation.navigate('DrawerOpen')}>
							<Image 
								source={ require('./../img/icon/menu.png') }
								style={{ height:20, width:20 }}
							/>
						</TouchableOpacity>
					</Left>
					<Body>
						<Title style={{ color:'white', textAlign:'center', alignContent:'center', alignItems:'center', width:206, fontSize:16 }} >Technical Support</Title>
					</Body>
					<Right>
		                <TouchableOpacity>
		                  <Button transparent onPress={() => this.props.navigation.navigate('Support Hub')}>
		                      <Image 
		                      source={ require('./../img/icon/arrow.png') }
		                      style={{ height:12, width:22 }}
		                    />
		                  </Button>
		                </TouchableOpacity>
		            </Right>
				</Header>
				<Content contentContainerStyle={ styles.bodyContainer }>

					<View style={styles.bodySubContainer}>
						<Animatable.View animation="flipInX">
							<TextInput
								style={ [styles.fieldTextarea, !this.state.validate_of_message ? styles.err:null] } 
								multiline={true}
								numberOfLines={6}
								minHeight={150}
								maxHeight={150}
								underlineColorAndroid='transparent'
								placeholder= 'Message'
								placeholderTextColor= '#ffffff'
								// value={ this.state.message }
								onChangeText = { (text) => this.setState({ message: text }) }
								minLength={10}
								maxLength={250}
							/>
						</Animatable.View>
						<View>
							<Text style={ !this.state.validate_of_message ? customStyles.usama:null }>{ this.state.validate_of_message_msg }</Text>
						</View>
					</View>

					<Animatable.View animation="flipInX" style={{  flex: 1, alignContent:'center', alignItems: 'center', marginTop: 10 }}>
						<Button 
							style={styles.touchBtnSubmit}
							onPress = {() => this.btnFormSubmit()}
						>
							<Text style={styles.btnButtonSubmit}>Submit</Text>
						</Button>
					</Animatable.View>
				</Content>

				<Footer style={{ backgroundColor:'transparent' }}>
					<FooterCopyRight />
				</Footer>


				<Modal
					transparent={ true }
					visible={this.state.isMessageModal}
					onRequestClose={ () => console.warn('this is') }
				>
					<View style={ customStyles.modalBody }>
						<View style={ customStyles.modalContainer }>
							<View style={ customStyles.modalBox }>
								<TouchableOpacity style={ customStyles.closeModalContainer } onPress={ () => {
										this.setState({
											isMessageModal: false
										})
									}}>
										<Icon name="times-circle" size={18} color="white" />
								</TouchableOpacity>
								<Text style={ customStyles.modalTextMessage }>
									{ this.state.isMessageModalText }
								</Text>
							</View>
						</View>
					</View>
				</Modal>

			</Container>
	  </ImageBackground>
    );
  } 
}

const styles = StyleSheet.create({

	bodyContainer: {
		// flex:1,
		alignItems:'center',
		justifyContent: 'center'
	},

	bodySubContainer:{
		flex:1,
		flexDirection: 'column',
		alignItems:'center',
		justifyContent: 'center',
		padding:20
	},

	inputField: {
		width: 300,
		backgroundColor: 'rgba(152, 152, 148, 0.7)',
		borderRadius: 25,
		paddingVertical:10,
		paddingHorizontal:20,
		fontSize: 16,
		color: '#ffffff',
		marginVertical: 5
	},

	fieldTextarea: {
		width: 300,
		textAlignVertical: 'top',
		backgroundColor: 'rgba(152, 152, 148, 0.7)',
		borderRadius: 25,
		fontSize: 16,
		color: '#ffffff',
		paddingVertical:12,
		paddingHorizontal:20,
		marginVertical: 5
	},

	touchBtnSubmit: {
		width: 300,
		backgroundColor: '#ed3134',
		borderRadius: 25,
		marginVertical: 5,
		paddingVertical: 12
	},

	btnButtonSubmit: {
		width: 300,
		fontSize: 16,
		fontWeight: '500',
		color:'#ffffff',
		textAlign: 'center'
	},

	err: {
		borderWidth:1,
		borderColor: 'red'
	}
});