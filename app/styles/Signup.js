import React, {StyleSheet} from 'react-native';

export default StyleSheet.create({

	err: {
		borderBottomColor: 'red',
		borderBottomWidth: 1,
		color: 'red'
	},

	bodyContainer: {
		flex: -1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#fff',
	},

	topHeader: {alignItems: 'center', marginVertical: 30 },

	mainText:{
		fontFamily: 'Roboto-Regular',
		fontWeight:'bold',
		fontSize: 20,
		color: '#ee3135',
	},

	classAvatar: {
		width:100, 
		height:100, 
		borderRadius: 100,
		backgroundColor: '#888',
		marginVertical: 30
	},

	/**
	 * Full Body INPUT Formatting
	 * ---------------------------------
	 *  
	 */

	fullInputContainerClass: { flexGrow: 1, flexDirection: 'row' , justifyContent: 'center'  },
	fullInputIconClass: { marginTop: 15 },

	dateDropDownPicker: {
        // borderBottomWidth: 1,
        // borderBottomColor: '#000',
        width: 300,
        // fontSize: 12,
		// fontWeight: '500',
	},
	
	inputField: {
		margin: 4,
		padding: 4,
		width: 110,
		height:40,
		fontFamily: 'Roboto-Bold',
		fontWeight: '500',
		fontSize: 13,
		color: '#000',
		borderBottomColor: '#000',
		borderBottomWidth: 1,
	},
	
	inputField_2: {
		margin: 5,
		padding: 5,
		width: 260,
		height:40,
		fontFamily: 'Roboto-Bold',
		fontWeight: '500',
		fontSize: 13,
		color: '#000',
		borderBottomColor: '#000',
		borderBottomWidth: 1,
	},

	inputDateField: {
        width: 260,
		paddingHorizontal: 16,
		marginVertical: 10,
		borderBottomColor: '#000',
		borderBottomWidth: 1,
	},

	touchLoginTButton: {
		width: 270,
		backgroundColor: '#ee3135',
		borderRadius: 25,
		paddingVertical: 12
	},

	textLoginTButton: {
		fontSize: 16,
		width: 270,
		fontWeight: '500',
		color:'#ffffff',
		textAlign: 'center'
	},
	
});