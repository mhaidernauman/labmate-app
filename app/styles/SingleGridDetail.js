import React, {StyleSheet} from 'react-native';


export default StyleSheet.create({

	container: { 
        flex:1 ,
        marginLeft:18,
        marginRight:18
    },

    header: { 
        marginTop: 15
    },

    header2: { 
        marginTop: 20,
        flexDirection: 'row' 
    },

    header3: { 
        marginTop: 15,
        flexDirection: 'row' 
    },

    body: { 
        marginTop: 15
    }
});