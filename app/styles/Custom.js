import React, {StyleSheet} from 'react-native';


export default StyleSheet.create({

	// DatePicker
	// datepicker: ,

	error_message: {  
		position: 'absolute', 
		right:10, 
		top:20, 
		color:'red', 
		backgroundColor:'white', 
		borderRadius:25, 
		width:150, 
		padding:5, 
		fontSize:10, 
		textAlign:'center', 
		borderWidth:1, 
		borderColor:'red' 
	},

	usama: {  
		color:'red', 
		padding:5, 
		fontSize:13, 
		textAlign:'center', 
		width:260
	},

	usama_two: {  
		color:'red', 
		padding:5, 
		fontSize:13, 
		textAlign:'center', 
		width:180,
		marginHorizontal:-20
	},

	// Modal Styles
	modalBody: { 
		flex: 1, 
		backgroundColor: 'rgba(52, 52, 52, 0.8)'
	},

	modalContainer: { 
		flexGrow:1, 
		flexDirection: 'column', 
		alignItems: 'center',  
		marginVertical: 200 
	},

	modalBox: {  
		backgroundColor: 'rgba(15, 15, 15, 0.7)', 
		width:300, 
		height: 180, 
		borderRadius:15 
	},

	closeModalContainer: {  
		marginRight:20, 
		marginTop:20,
		alignSelf: 'flex-end' 
	},

	modalTextMessage: { 
		marginTop: 80, 
		textAlign:'center', 
		fontFamily: 'Roboto-Bold', 
		fontWeight: 'bold', 
		color: 'white', 
		fontSize: 14, 
		marginHorizontal:20 
	},

	closeModalContainer: {  
		position:'absolute',
		top:12,
		right:15,
		zIndex:2
	},

});