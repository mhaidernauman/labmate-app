import React, {StyleSheet} from 'react-native';


export default StyleSheet.create({

	bodyContainer: {
		// flex:1,
		alignItems:'center',
		justifyContent: 'center'
	},

	bodySubContainer:{
		flex:1,
		flexDirection: 'column',
		alignItems:'center',
		justifyContent: 'center',
		padding:20
	},

	inputField: {
		width: 310,
		backgroundColor: 'rgba(152, 152, 148, 0.7)',
		borderRadius: 25,
		paddingVertical:12,
		paddingHorizontal:20,
		fontSize: 16,
		color: '#ffffff',
		marginVertical: 10
	},

	fieldTextarea: {
		width: 310,
		textAlignVertical: 'top',
		backgroundColor: 'rgba(152, 152, 148, 0.7)',
		borderRadius: 25,
		fontSize: 16,
		color: '#ffffff',
		paddingVertical:12,
		paddingHorizontal:20,
		marginVertical: 10
	},

	touchBtnSubmit: {
		width: 310,
		backgroundColor: '#ed3134',
		borderRadius: 25,
		marginVertical: 10,
		paddingVertical: 12,
		elevation:3
	},

	btnButtonSubmit: {
		fontSize: 16,
		fontWeight: '500',
		color:'#ffffff',
		textAlign: 'center',
		width: 310

	},

	footerCopyright: {
		paddingTop: 18,
		alignContent:'center', 
		alignItems:'center'
	},

	err: {
		borderWidth:1,
		borderColor: 'red'
	}
});