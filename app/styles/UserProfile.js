import React, {StyleSheet} from 'react-native';


export default StyleSheet.create({

	bodyContainer: {
		// flex:1,
		alignItems:'center',
		justifyContent: 'center'
	},

	bodySubContainer:{
		flex:1,
		flexDirection: 'column',
		alignItems:'center',
		justifyContent: 'center',
		padding:20
	},
	
	topHeader: {alignItems: 'center', borderBottomWidth:2,
		borderColor:'lightgrey', width:320, height:180 },

	mainText:{
		fontFamily: 'Roboto-Regular',
		fontWeight:'bold',
		fontSize: 20,
		marginVertical:15,
		color: '#ee3135'
	},

	iconPencil:{
		position:'absolute',
		right:105,
		top:75,
		height:32,
		width:32,
		backgroundColor:'black',
		borderRadius:25
	},
	
	header3: { 
        flexDirection: 'row'
    },


	footerCopyright: {
		paddingTop: 18,
		alignContent:'center', 
		alignItems:'center'
	},

	err: {
		borderWidth:1,
		borderColor: 'red',
		backgroundColor:'white'
	}
});